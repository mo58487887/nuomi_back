declare module MyTyping {
	type ExParent<P> = {
		[K in keyof P]?: P[K]
	}

	interface IAnyObject {
		[x: string]: any
	}

	interface returnData {
		code: number
		data: IAnyObject | null
		msg: string
	}
}
