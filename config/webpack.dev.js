const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const config = require("./index.js");
const webpack = require("webpack");
module.exports = merge(common, {
    mode: "development",
    devtool: "cheap-module-eval-source-map",
    devServer: {
        proxy: {
            "/": {
                target: config.baseUrl.dev,
                secure: false,
                changeOrigin: true,
                cookieDomainRewrite: "localhost"
            }
		},
		host: "192.168.0.7",
        port: 8281,
        open: true,
        hot: true,
        historyApiFallback: {
            rewrites: [{ from: /./, to: "/" }]
        }
    },
    plugins: [new webpack.HotModuleReplacementPlugin()]
});
