import * as React from "react";
import { TableColumn, TableApi } from "../../components/table";
import withTable from "../../hoc/withTable";
import ExpressInfo from "./components/ExpressInfo";
import { formatTime } from "../../libs/util";
import DownloadExcel from "./components/DonwloadExcel";

const columns: Array<TableColumn<MyTyping.IAnyObject>> = [
	{
		title: "订单编号",
		dataIndex: "ordersn",
		renderInTable: true,
	},
	{
		title: "订单时间",
		dataIndex: "createTime",
		renderInTable: true,
		render: t => formatTime(t.time)
	},
	{
		title: "订单金额",
		dataIndex: "orderMoney",
		renderInTable: true
	},
	{
		title: "收货地址",
		dataIndex: "address",
		renderInTable: true,
		render: (t, r) => <span>{`${r.province}${r.city}${r.area}${r.address}`}</span>
	},
	{
		title: "收货人",
		dataIndex: "name",
		renderInTable: true,
	},
	{
		title: "联系方式",
		dataIndex: "phone",
		renderInTable: true
	},
	{
		title: "订单商品",
		dataIndex: "orderGoodsInfo",
		renderInTable: true,
		bindRender: (t, r, i, s) => <ExpressInfo record={r} table={s}></ExpressInfo>
	}
]

const api: TableApi = {
	list: {
		url: "/order/queryOrderListByshopId"
	}
} 

const rowKey = "ordersn"

export default withTable({ columns, api, rowKey, CustomHeaderButton: DownloadExcel });