import React = require("react");
import { Button } from "antd";
import { download } from "../../../../libs/util";


export default class DownloadExcel extends React.Component {
	render() {
		return <Button style={{marginLeft: "10px"}} onClick={() => download("/order/export")}>导出</Button>
	}
}