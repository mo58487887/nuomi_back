import * as React from "react";
import { Button, Modal, message, AutoComplete, Input, Row, Col } from "antd";
import ajax from "../../../../libs/request";

interface InputState {
    visible: boolean;
    /** 用于自动搜索完成的 */
    logiAutoSearchDatasource: string[];
    /** 物流公司输入 */
    loginame: string;
    /** 快递物流编号的输入 */

    loginum: string;
}

interface InputProps {
    /** 物流公司列表 */
    logiArr: logiInfo[];
    /** 订单商品id */
    ogid: string;
    /** 物流公司输入 */
    loginame: string;
    /** 快递物流编号的输入 */

    loginum: string;
}

interface logiInfo {
    loginame: string;
    logicode: string;
}

export default class ExpressInput extends React.Component<
    InputProps,
    InputState
> {
    constructor(props: InputProps) {
        super(props);
        this.state = {
            visible: false,
            logiAutoSearchDatasource: [],
            loginame: props.loginame || "",
            loginum: props.loginum || ""
        };
    }

    openEdit = () => {
        this.setState({
            visible: true
        });
    };

    closeEdit = () => {
        this.setState({
            visible: false
        });
    };

    /** 物流公司 */
    onAutoCompoleteSearch = (value: string) => {
		console.log(this.props.logiArr.filter(_ => _.loginame.indexOf("盛丰") > -1))
        this.setState({
            loginame: value,
            logiAutoSearchDatasource: this.props.logiArr
                .filter(_ => _.loginame.indexOf(value) > -1)
                .map(_ => _.loginame)
        });
    };

    setLoginum = ({
        target: { value }
    }: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            loginum: value
        });
    };

    /** 提交 */
    submitLogi = () => {
        const { loginame, loginum } = this.state;
        const { ogid, logiArr } = this.props;
        if (!logiArr.find(_ => _.loginame === loginame)) {
            message.warning("没有该物流公司");
            return;
        }
        if (
            loginame === this.props.loginame &&
            loginum === this.props.loginum
        ) {
            this.setState({
                visible: false
            });
            return;
        }
        ajax({
            url: "/order/updateLogiByOgid",
            data: {
                ogid,
                loginame,
                logicode: logiArr.find(_ => _.loginame === loginame).logicode,
                loginum
            }
        }).then(res => {
            if (res.code === 0) {
                message.success("修改成功");
                this.setState({
                    visible: false
                });
            } else {
				message.warning("修改失败")
			}
        });
    };

    render() {
        const {
            visible,
            logiAutoSearchDatasource,
            loginame,
            loginum
        } = this.state;
        return (
            <div>
                <Button size="small" onClick={this.openEdit}>
                    快递修改
                </Button>
                <Modal
                    visible={visible}
                    title="修改快递"
                    onCancel={this.closeEdit}
                    onOk={this.submitLogi}
                >
                    <div>
                        <Row>
                            <Col span={8}>快递公司</Col>
                            <Col span={16}>
                                <AutoComplete
                                    onChange={this.onAutoCompoleteSearch}
                                    dataSource={logiAutoSearchDatasource}
                                    value={loginame}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col span={8}>快递编号</Col>
                            <Col span={16}>
                                <Input
                                    onChange={this.setLoginum}
                                    value={loginum}
                                />
                            </Col>
                        </Row>
                    </div>
                </Modal>
            </div>
        );
    }
}
