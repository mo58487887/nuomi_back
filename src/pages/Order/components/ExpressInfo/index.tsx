import * as React from "react";
import ajax from "../../../../libs/request";
import { Button, Modal, message, List, Avatar, AutoComplete, Input } from "antd";
import ExpressInput from "../ExpressInput";

interface Props {
    /** 该项数据 */
    record: any;
    /** Table实例 */
    table: any;
}

interface State {
    visible: boolean;
	goodsList: OrderGoods[];
	/** 物流公司列表 */
	logiArr: logiInfo[]
}

interface LogiInput {
	lognum: string
	logname: string
}

interface OrderGoods {
    attrName: string;
    goodsImg: string;
    goodsName: string;
    /** 商品数量 */
    goodsNum: number;
    /** 供货商 */
    goodsSupplier: string;
    /** 物流编号 */
	logisticsNum: null;
	/** 物流公司名称 */
	loginame: string
    /** 物流状态0 在路上 1已收货 2代发货 */
    logisticsState: 0 | 1 | 2;
	shopPrice: string;
	/** 订单商品id */
	ogid: string
	
}

interface logiInfo {
	loginame: string
	logicode: string
}


export default class ExpressInfo extends React.Component<Props, State> {
    state: State = {
        visible: false,
		goodsList: [],
		logiArr: [],
    };

    // componentDidMount() {
    // 	ajax({
    // 		url: "/"
    // 	})
    // }

    onOpenModal = () => {
        ajax({
            url: "/order/queryOrderGoodsListByOrderSn",
            data: { orderSn: this.props.record.ordersn }
        }).then(res => {
            if (res.code === 0) {
                this.setState({
                    visible: true,
                    goodsList: res.data
                });
            } else {
                message.warning(res.message);
            }
		});
		ajax({
			url: "/order/queryLogisticsList"
		}).then(res => {
			if (res.code === 0) {
				this.setState({
					logiArr: res.data
				})
			}
		})
    };

	/** 关闭弹窗 */
    onCloseModal = () => {
        this.setState({
            visible: false
        });
    };

    render() {
        const { record } = this.props;
        const { visible, goodsList, logiArr } = this.state;
        return (
            <div>
                <Button size="small" type="primary" onClick={this.onOpenModal}>
                    查询
                </Button>
                <Modal
                    visible={visible}
                    title="查看商品"
                    onCancel={this.onCloseModal}
                >
                    <List
                        itemLayout="horizontal"
                        dataSource={goodsList}
                        renderItem={item => (
                            <List.Item>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar
                                            size="large"
                                            src={item.goodsImg}
                                        />
                                    }
                                    title={
                                        <div>
                                            <div>{item.goodsName}</div>
                                            <div>{item.goodsSupplier}</div>
                                            <div>数量:{item.goodsNum}</div>
                                            <div>规格:{item.attrName}</div>
                                        </div>
                                    }
                                    description={
										<div style={{display: "flex"}}>
											<div style={{color: "#c50000"}}>
												{item.logisticsState === 0
													? `配送中----${item.loginame}::${item.logisticsNum}`
													: item.logisticsState === 1
													? `已收货----${item.loginame}::${item.logisticsNum}`
													: "代发货"}
											</div>
											<ExpressInput logiArr={logiArr} ogid={item.ogid} loginum={item.logisticsNum} loginame={item.loginame} ></ExpressInput>
										</div>
									}
                                />
                            </List.Item>
                        )}
                    />
                    ,
                </Modal>
            </div>
        );
    }
}
