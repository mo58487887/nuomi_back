import * as React from "react"
import ajax from "../../../../libs/request";

/** 分类的Props */
interface CategoryProps {
    /** Form传递的值,categoryId */
    value?: string | number;
    /** 传递改变 */
    onChange?: (...param: any[]) => void;
}

interface CategoryState {
    /** 一级分类 */
    firstCate: any[];
    /** 二级分类 */
    secondCate: any[];
    /** 三级分类 */
    thirdCate: any[];
    /** 一级ID */
    firstCateId: string | number;
    /** 二级ID */
    secondCateId: string | number;
    /** 三级ID,也是最终值 */
    categoryId: string | number;
}

export default class GoodsCategoryChoose extends React.Component<
    CategoryProps,
    CategoryState
> {
    state: CategoryState = {
        firstCate: [],
        secondCate: [],
        thirdCate: [],
        firstCateId: "",
        secondCateId: "",
        categoryId: ""
    };

    constructor(props: CategoryProps) {
        super(props);
    }

    componentDidMount() {
        if (this.props.value) {
            this.setState({
                categoryId: this.props.value
            });
        }
        ajax({
            url: "/goodsCategory/queryGoodsCategoryListBycatId",
            data: { catId: 0 }
        }).then(res => {
            this.setState({
                firstCate: res
            });
        });
    }
    /** 继续下一级别的点击 */
    onFirstClick = (id: string | number) => {
		if (this.state.firstCateId !== id) {
			ajax({
				url: "/goodsCategory/queryGoodsCategoryListBycatId",
				data: { catId: id }
			}).then(res => {
				this.setState({
					secondCate: res,
					firstCateId: id,
					thirdCate: []
				});
			});
		}
    };
    onSecondClick = (id: string | number) => {
		if (this.state.secondCateId !== id) {
			ajax({
				url: "/goodsCategory/queryGoodsCategoryListBycatId",
				data: { catId: id }
			}).then(res => {
				this.setState({
					thirdCate: res,
					secondCateId: id
				});
			});
		}
    };
    /** 最终级点击事件 */
    onThirdClick = (id: string | number) => {
        this.setState({
            categoryId: id
        });
        if (this.props.onChange) {
            this.props.onChange(id);
        }
    };
    render() {
        const {
            firstCate,
            firstCateId,
            secondCate,
            secondCateId,
            categoryId,
            thirdCate
        } = this.state;
        const borderColorStyle = { borderColor: "#0083ff" };
        return (
            <div className="goods-cate-choose">
                <div className="cate-choose-area">
                    <div className="cate-choose-label">一级分类</div>
                    {firstCate.map(_ => (
                        <div
                            onClick={() => this.onFirstClick(_.catid)}
                            style={
                                _.catid === firstCateId ? borderColorStyle : {}
                            }
                            key={_.catid}
                            className="cate-item"
                        >
                            {_.catName}
                        </div>
                    ))}
                </div>
                {secondCate.length > 0 && (
                    <div className="cate-choose-area">
                        <div className="cate-choose-label">二级分类</div>
                        {secondCate.map(_ => (
                            <div
                                onClick={() => this.onSecondClick(_.catid)}
                                style={
                                    _.catid === secondCateId
                                        ? borderColorStyle
                                        : {}
                                }
                                key={_.catid}
                                className="cate-item"
                            >
                                {_.catName}
                            </div>
                        ))}
                    </div>
                )}
                {thirdCate.length > 0 && (
                    <div className="cate-choose-area">
                        <div className="cate-choose-label">三级分类</div>
                        {thirdCate.map(_ => (
                            <div
                                onClick={() => this.onThirdClick(_.catid)}
                                style={
                                    _.catid === categoryId
                                        ? borderColorStyle
                                        : {}
                                }
                                key={_.catid}
                                className="cate-item"
                            >
                                {_.catName}
                            </div>
                        ))}
                    </div>
                )}
            </div>
        );
    }
}