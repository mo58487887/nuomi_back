import * as React from "react";
import { RcFile } from "antd/lib/upload";
import { randomString } from "../../../../libs/util";
import { message, Input, Icon, Button, Row, Col } from "antd";
import MyUpload from "../../../../components/upload";

interface GoodsSpecProps {
	value?: string | string[] 
	onChange?: (...param: any[]) => void
}

interface GoodsSpecState {
	/** 当前正在输入的规格 */
	currentSpecInfo: {
		/** 一级规格 */
		specFirst: string
		/** 二级规格 */
		specSecond: string
		/** 规格价格 */
		specPrice: number | ""
		/** 规格库存 */
		specStock: number | ""
		/** 规格图片 */
		specImage: {
			/** 文件名称,随机字符串生成前缀 */
			name?: string
			/** File */
			file?: RcFile
			/** base64编码的图片资源 */
			dataUrl?: string
		}
	}
	/** 已输入的规格列表 */
	goodsSpec: GoodsSpecState["currentSpecInfo"][]
	isInputting: boolean
}

export default class GoodsSpecInput extends React.Component<GoodsSpecProps, GoodsSpecState> {
	constructor(props: GoodsSpecProps) {
		super(props)
	}

	state: GoodsSpecState = {
		currentSpecInfo: {
			specFirst: "",
			specSecond: "",
			specPrice: "",
			specStock: "",
			specImage: {}
		},
		goodsSpec: [],
		isInputting: true
	}

	/** 规格单图的onChange事件 */
	onSpecImageChange = ({fileList}: {fileList: RcFile[]}) => {
		const reader = new FileReader()
		// 规格单图
		const f = fileList[0];
		reader.readAsDataURL(f)
		reader.onload = () => {
			this.setState(pre => ({
				currentSpecInfo: Object.assign({}, pre.currentSpecInfo, {
					specImage: {
						dataUrl: reader.result,
						file: f,
						name: randomString(8) + f.name
					}
				})
			}))
		}
	}

	/** 规格输入控制 */
	onSpecInput = ({target: {value}}: React.ChangeEvent<HTMLInputElement>, key: string) =>  {
		this.setState(pre => ({
			currentSpecInfo: Object.assign({}, pre.currentSpecInfo, {[key]: value})
		}))
	}

	/** 删除规格的图片 */
	onRemoveSpecImage = () => { 
		this.setState(pre => ({
			currentSpecInfo: Object.assign({}, pre.currentSpecInfo, {specImage: {}})
		}))
	}

	/** 插入规格 */
	onInsertSpec = () => {
		const {currentSpecInfo} = this.state
		const {specFirst, specImage, specPrice, specSecond, specStock} = currentSpecInfo
		if (!specFirst || !specImage || !specImage.file || !specPrice || !specSecond || !specStock) {
			message.warning("规格不能存在空项")
			return
		}
		if ((!parseFloat(specPrice.toString()) && parseFloat(specPrice.toString()) !== 0) || parseFloat(specPrice.toString()) < 0) {
			message.warning("价格不正确")
			return
		}
		if ((!parseInt(specStock.toString()) && parseInt(specStock.toString()) !== 0) || parseInt(specStock.toString()) < 0) {
			message.warning("库存数量输入不正确")
			return
		}
		this.setState(pre => ({
			goodsSpec: pre.goodsSpec.concat([currentSpecInfo]),
			isInputting: false
		}), () => {
			this.setState({
				currentSpecInfo: {
					specFirst: "",
				    specSecond: "",
				    specPrice: "",
				    specStock: "",
				    specImage: {}
				},
				isInputting: true
			})
			this.onChange()
		})
	}

	/** 移除某一项规格 */
	removeSpecItem = (i: number) => {
		this.setState(pre => ({
			goodsSpec: pre.goodsSpec.filter((_, idx) => idx !== i)
		}), this.onChange)
	}

	onChange = () => {
		if (this.props.onChange) {
			this.props.onChange(this.state.goodsSpec)
		}
	}

	render() {
		const {currentSpecInfo, goodsSpec, isInputting} = this.state
		const {specFirst, specSecond, specPrice, specStock, specImage} = currentSpecInfo
		return (
			<div className="spec-input-wrap">
				<Row className="spec-input">
					<Col span="6" className="spec-label">一级规格</Col>
					<Col span="18">
						<Input placeholder="请输入一级规格" value={specFirst} onChange={e => this.onSpecInput(e, "specFirst")} />
					</Col>
				</Row>
				<Row className="spec-input">
					<Col span="6" className="spec-label">二级规格</Col>
					<Col span="18">
						<Input placeholder="请输入二级规格" value={specSecond} onChange={e => this.onSpecInput(e, "specSecond")} />
					</Col>
				</Row>
				<Row className="spec-input">
					<Col span="6" className="spec-label">规格价格</Col>
					<Col span="18">
						<Input placeholder="请输入规格价格" value={specPrice} onChange={e => this.onSpecInput(e, "specPrice")} />
					</Col>
				</Row>
				<Row className="spec-input">
					<Col span="6" className="spec-label">规格库存</Col>
					<Col span="18">
						<Input placeholder="请输入规格库存" value={specStock} onChange={e => this.onSpecInput(e, "specStock")} />
					</Col>
				</Row>
				<Row className="spec-input">
					<Col span="6" className="spec-label">规格图片</Col>
					<Col span="18" className="image-input">
						{/* {
							specImage.dataUrl &&  <span className="img-wrap"><img src={specImage.dataUrl} /><span onClick={this.onRemoveSpecImage}><Icon type="delete" /></span></span>
						} */}
						{
							isInputting && <MyUpload value={specImage.dataUrl} multiple={false} onChange={this.onSpecImageChange} />
						}
						{/* {
							!specImage.dataUrl ? 
							<MyUpload value={specImage.dataUrl} multiple={false} onChange={this.onSpecImageChange} />
							: <span className="img-wrap"><img src={specImage.dataUrl} /><span onClick={this.onRemoveSpecImage}><Icon type="delete" /></span></span>
						} */}
					</Col>
					{/* <input placeholder="请输入规格图片地址,只允许输入一张图片地址" value={specImage} onChange={e => this.onSpecInput(e, "specImage")}></input> */}
				</Row>
				<Button type="primary" onClick={this.onInsertSpec}>插入规格</Button>
				{
					goodsSpec.length > 0 && 
					goodsSpec.map((_, i) => <div className="spec-item" key={`goodsSpec-${i}`}>
							<span>{_.specFirst}</span>
							<span>{_.specSecond}</span>
							<span>{_.specPrice}</span>
							<span>{_.specStock}</span>
							<span><img src={_.specImage.dataUrl}></img></span>
							<span onClick={() => this.removeSpecItem(i)} style={{color: '#c50000', cursor: 'pointer'}}>删除</span>
						</div>)
				}
			</div>
		)
	}
}