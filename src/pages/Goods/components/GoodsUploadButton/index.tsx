import * as React from "react";
import { Button } from "antd/lib/radio";
import { Modal, message } from "antd";
import MyUpload from "../../../../components/upload";
import { RcFile } from "antd/lib/upload";
import ajax from "../../../../libs/request";

interface State {
	/** modal可见 */
	modalVisible: boolean
	/** file */
	file: RcFile | null
	/** modal的确定按钮加载中 */
	confirmLoading: boolean
}

interface Props {

}

export default class GoodsUploadButton extends React.Component<Props, State> {

	state: State = {
		modalVisible: false,
		file: null,
		confirmLoading: false
	}
	
	openModal = () => {
		this.setState({modalVisible: true})
	}

	closeModal = () => {
		this.setState({modalVisible: false})
	}

	submit = () => {
		const {file} = this.state
		if (!file) {
			message.warning("请先选择excel文件")
		}
		this.setState({
			confirmLoading: true
		})
		ajax({
			url: "/goods/readExcel",
			data: {
				excel: [file]
			},
			dataType: "text"
		}).then(res => {
			if (res === "succ") {
				message.success("导入成功")
				this.setState({
					confirmLoading: false,
					modalVisible: false
				})
			} else {
				this.setState({
					confirmLoading: false
				})
				message.warning(res)
			}
		})
	}

	getFile = ({fileList}: {fileList: RcFile[]}) => {
		this.setState({
			file: fileList[0]
		})
	}

	render() {
		const {modalVisible, confirmLoading} = this.state
		return (
			<div>
				<Button style={{marginLeft: "10px"}} onClick={this.openModal}>导入</Button>
				<Modal visible={modalVisible}
						title="导入excel"
						onCancel={this.closeModal}
						onOk={this.submit}
						confirmLoading={confirmLoading}>
					<div>
						<MyUpload multiple={false} accept=".xls, .xlsx" onChange={this.getFile} />
					</div>
				</Modal>
			</div>
		)
	}
}