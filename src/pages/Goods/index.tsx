import * as React from "react";
import { TableColumn, TableApi } from "../../components/table";
import withTable from "../../hoc/withTable";
import { randomString } from "../../libs/util";
import GoodsSpecInput from "./components/GoodsSpecInput";
import GoodsCategoryChoose from "./components/GoodsCategoryChoose";
import "./index.less";
import GoodsUploadButton from "./components/GoodsUploadButton";

const columns: Array<TableColumn<MyTyping.IAnyObject>> = [
    {
        title: "商品分类",
        dataIndex: "categoryId",
        input: {
            type: "render",
            setting: {
                rules: [
                    {
                        required: true,
                        message: "请选择商品所在分类"
                    }
                ]
            },
            editRender: <GoodsCategoryChoose />
        }
    },
    {
        title: "商品封面",
        dataIndex: "goodsImages",
        // renderInTable: true,
        // renderInInfo: true,
        render: t => (
            <img style={{ width: "100px", height: "100px" }} src={t} />
        ),
        input: {
            type: "upload",
            setting: {
                rules: [
                    {
                        required: true,
                        message: "请上传商品封面"
                    }
                ]
            }
        }
    },
    {
        title: "商品名称",
        dataIndex: "goodsName",
        renderInTable: true,
        input: {
            type: "input",
            placeholder: "请输入商品名称",
            setting: {
                rules: [
                    {
                        required: true,
                        message: "请填写商品名称"
                    }
                ]
            }
        }
    },
    {
        title: "商品详情",
        dataIndex: "goodsDetail",
        input: {
            type: "upload",
            placeholder: "请输入商品详情",
            uploadMultiple: true
        }
    },
    {
        title: "商品规格",
        dataIndex: "goodsSpec",
        input: {
            type: "render",
            placeholder: "请输入商品规格",
            editRender: <GoodsSpecInput />,
            setting: {
                rules: [
                    {
                        validator: (rule, value, callback) => {
                            if (value.length === 0) {
                                callback(new Error("请最少输入一项规格"));
                            } else {
                                callback();
                            }
                        }
                    }
                ]
            }
        }
    },
    {
        title: "是否上平台",
        dataIndex: "isOnPlatform",
        input: {
            type: "radio",
            options: [
                {
                    label: "上平台",
                    value: 1
                },
                {
                    label: "暂时不上",
                    value: 0
                }
			],
			setting: {
				initialValue: 1
			}
        }
    },
    {
        title: "是否上架",
        dataIndex: "isOnline",
        input: {
            type: "radio",
            options: [
                {
                    label: "上架",
                    value: 1
                },
                {
                    label: "暂不",
                    value: 0
                }
            ],
			setting: {
				initialValue: 1
			}
        }
    },
    {
        title: "是否上架",
		dataIndex: "isonsale",
		renderInTable: true,
		render: t => t === 1 ? <span style={{color: "rgb(45, 157, 255)"}}>已上</span> : <span style={{color: "rgb(255, 77, 79)"}}>未上</span>
    },
    {
        title: "是否上平台",
		dataIndex: "isalonsale",
		renderInTable: true,
		render: t => t === 1 ? <span style={{color: "rgb(45, 157, 255)"}}>已上</span> : <span style={{color: "rgb(255, 77, 79)"}}>未上</span>
    },
    {
        title: "是否海外",
        dataIndex: "isOverseas",
        input: {
            type: "radio",
            options: [
                {
                    label: "海外商品",
                    value: 1
                },
                {
                    label: "国内商品",
                    value: 0
                }
            ],
			setting: {
				initialValue: 1
			}
        }
	},
    {
        title: "是否包邮",
        dataIndex: "isShipping",
        input: {
            type: "radio",
            options: [
                {
                    label: "包邮",
                    value: 1
                },
                {
                    label: "不包邮",
                    value: 0
                }
            ],
			setting: {
				initialValue: 1
			}
        }
	},
	{
		title: "供应商",
		dataIndex: "sId",
		input: {
			type: "chooseId",
			chooseOptions: {
				columns: [
					{
						title: "供货商",
						dataIndex: "sName",
						renderInTable: true
					}
				],
				api: {
					list: {
						url: "/goodsSupplier/getGoodsSupplierList"
					}
				},
				name: "sName",
				chooseKey: "sId",
				placeholder: "请选择供货商"
			},
		}
	}
];

const api: TableApi = {
    list: {
        url: "/goods/queryGoodsListByShopId"
    },
    // edit: {
    // 	url: "/goodsSupplier/addGoodsSupplier"
    // },
    create: {
        url: "/goods/addGoods",
        preHandle: preHandle
    }
};

function preHandle(data: any) {
    return Object.assign(
        {},
        data,
		{ goodsSn: "nm" + randomString(8) },
		{goodsImages: data.goodsImages.fileList},
		{goodsDetail: data.goodsDetail.fileList},
        {
            specImages: data.goodsSpec.map((_: any) => ({
                file: _.specImage.file,
                name: _.specImage.name
            }))
        },
        {
            goodsSpec: JSON.stringify(data.goodsSpec.map((_: any) => ({
                ..._,
                specImage: _.specImage.name
            })))
        }
    );
}

export default withTable({ columns, api, rowKey: "gId", CustomHeaderButton: GoodsUploadButton });
