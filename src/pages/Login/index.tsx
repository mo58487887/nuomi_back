import * as React from 'react'
import {
  Form, Icon, Input, Button, Checkbox,
} from 'antd';
import './index.less'
// import ajax from '../../libs/request'
import {setToken} from '../../actions'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { FormComponentProps } from 'antd/lib/form';
import ajax from '../../libs/request';
const FormItem = Form.Item;

const mapStateToProps = (state: any) => {
  return {
    token: state.user.token
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    setToken: (token: string) => {
      dispatch(setToken(token))
    },
  }
}

interface Props extends RouteComponentProps , FormComponentProps {
	token: string
	setToken: (token: string) => void
}

class LoginForm extends React.Component<Props> {

  handleSubmit = (e: { preventDefault: () => void; }) => {
	e.preventDefault();
	// this.props.history.push('/')
    this.props.form.validateFields((err, values) => {
      if (!err) {
        ajax({
          url: '/shopUser/submitLogin',
          data: values
        }).then(res => {
          if (res.code === 0) {
			  console.log(res)
            this.props.setToken(res.body)
            this.props.history.push('/')
          }
        })
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const backgroundImage = 'url("http://tuimeizi.cn/random?w=1920&h=1080&s=0")'
    return (
      <div className="login-out-wrap" style={{backgroundImage}}>
        <div className="login-wrap">
          <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator('logname', {
                rules: [{ required: true, message: '请输入登录用户名' }],
                initialValue: 'nuomibaby'
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户名" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('logpass', {
                rules: [{ required: true, message: '请输入密码' }],
              })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
              )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit" className="login-form-button">
                登录
              </Button>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create<Props>()(LoginForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(WrappedNormalLoginForm))