import * as React from "react";
import { TableColumn, TableApi } from "../../components/table";
import withTable from "../../hoc/withTable";
import DownloadExcel from "../Order/components/DonwloadExcel";

const columns: Array<TableColumn<MyTyping.IAnyObject>> = [
	{
		title: "供货商名称",
		dataIndex: "sName",
		renderInTable: true,
		input: {
			type: "input",
			placeholder: "请输入供货商名称",
			setting: {
				rules: [
					{
						required: true,
						message: "请填写供货商名称"
					}
				]
			}
		}
	},
	{
		title: "供货地址",
		dataIndex: "sAddress",
		renderInTable: true,
		input: {
			type: "input",
			placeholder: "请输入供货商所在地",
			setting: {
				rules: [
					{
						required: true,
						message: "请填写供货商所在地"
					}
				]
			}
		}
	},
	{
		title: "联系人",
		dataIndex: "sPerson",
		renderInTable: true,
		input: {
			type: "input",
			placeholder: "请输入供货商联系人",
			setting: {
				rules: [
					{
						required: true,
						message: "请填写供货商联系人"
					}
				]
			}
		}
	},
	{
		title: "供货商邮编",
		dataIndex: "sCode",
		renderInTable: true,
		input: {
			type: "input",
			placeholder: "请输入供货商邮编"
		}
	},
	{
		title: "供货商传真",
		dataIndex: "sFax",
		renderInTable: true,
		input: {
			type: "input",
			placeholder: "请输入供货商传真"
		}
	},
	{
		title: "供货商备注",
		dataIndex: "sDesc",
		renderInTable: true,
		input: {
			type: "input",
			placeholder: "请输入供货商备注"
		}
	},
]

const api: TableApi = {
	list: {
		url: "/goodsSupplier/queryGoodsSupplierList"
	},
	// edit: {
	// 	url: "/goodsSupplier/addGoodsSupplier"
	// },
	create: {
		url: "/goodsSupplier/addGoodsSupplier"
	}
}

export default withTable({columns, api, rowKey: 'sId'})