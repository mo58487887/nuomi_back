import * as React from "react";
// import { withRouter } from "react-router-dom";
// import { connect } from 'react-redux'
import Main from "./components/main";
import "./app.less";
import ajax from "./libs/request";
import { withRouter, RouteComponentProps } from "react-router";

class App extends React.Component<RouteComponentProps, {}> {
    constructor(props: RouteComponentProps) {
        super(props);
    }

    render() {
        return <Main>{this.props.children}</Main>;
    }
}

export default  withRouter(App)