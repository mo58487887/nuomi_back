import * as React from 'react'
import './index.less'
import { Row, Col } from 'antd';
import { TableColumn } from '../table';

interface Props {
	columns: Array<TableColumn<MyTyping.IAnyObject>>
	data: MyTyping.IAnyObject
}


export default class Info extends React.Component<Props> {
  constructor(props: Props) {
    super(props)
  }

  render() {
    const data = this.props.data
    const columns: (TableColumn<MyTyping.IAnyObject> & {
		data: any;
	})[]  = this.props.columns.filter(_ => _.renderInInfo).map(_ => {
          return Object.assign({}, _, {data: data[_.dataIndex]})
      })
    return (
      <div  className="info-wrap" style={{minWidth: 
        // columns.length > 10 ? '1000px' : 
        '500px'}}>
        {
          columns.map((_, i) => <Row className="info-item"
            key={`info-item_${i}`}>
            <Col span={6} className="info-item-title">{_.title}</Col>
            <Col span={18} className="info-item-data">{_.render ? _.render(_.data, data, i) : _.data}</Col>
          </Row>)
        }
      </div>
    )
  }
}