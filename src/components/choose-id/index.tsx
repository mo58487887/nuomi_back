import * as React from 'react'
import { Button, Modal} from 'antd';
import { MyTable, TableColumn, TableApi } from '../table'

export interface ChooseIdProps {
	/** 返回值{id, name} */
	value?: formValue
	/** 选择列表的column */
	columns?: Array<TableColumn<MyTyping.IAnyObject>>
	/** 选择列表的api值,一般只需要提供list */
	api?: TableApi
	/** 展示名称的key */
	name?: string
	/** 提示语 */
	placeholder?: string
	/** 选择的Id的key值 */
	chooseKey?: string
	/** onChange */
	onChange?: (...param: any[]) => void
}

type formValue =  {
	id: string
	name: string
} | string | number

interface State {
	/** 展示名称 */
	name: string
	/** form值,有可能是具体值,也有可能是选择后的值 */
	id: formValue,
	/** 弹窗显示 */
	visible: boolean,
	columns: Array<TableColumn<MyTyping.IAnyObject>>
}

/**
 *  @description 选择id
 *  @param {Array} columns 选择列表
 *  @param {String} name 展示名称的key
 *  @param {Object} value 返回值{id, name} 
 *  @param {Object} api api{list{}}
 */
export class ChooseId extends React.Component<ChooseIdProps, State> {
//   static getDerivedStateFromProps(nextProps: Props) {
//     if ('value' in nextProps) {
//       return {
//         ...(nextProps.value || {}),
//       };
//     }
//     return null;
//   }
  constructor(props: ChooseIdProps) {
    super(props)
    const value = props.value
    const columns = props.columns.concat([{
      title: '选择',
      dataIndex: 'choose',
      renderInTable: true,
      render: (text, record) => <Button 
          size="small" 
          onClick={() => this.onChoose(record)}>选择</Button>
	}])
	const name: string | number = (typeof value === "string" || typeof value === "number") ? value : ""
    this.state = {
      name: `${name}` || props.placeholder || '点击选择',
      id: value || '',
      visible: false,
      columns
    }
  }
  /** 点击事件 */
  onChoose = (record: any) => {
    this.setState({
      name: record[this.props.name],
      id: record[this.props.chooseKey],
      visible: false
    })
    if (this.props.onChange) {
      this.props.onChange(record[this.props.chooseKey])
    }
  } 
  /** 开选择 */
  onOpenModal = () => {
    this.setState({
      visible: true
    })
  }
  /** 关闭选择 */
  closeModal = () => {
    this.setState({
      visible: false
    })
  }
  render() {
    const {name, id, visible, columns} = this.state
    return (
      <span>
        <Button style = {{height: '32px'}} block onClick={this.onOpenModal}>{name}</Button>
        <Modal visible={visible} destroyOnClose onCancel={this.closeModal} footer={null}>
        {
          visible && <MyTable columns={columns} api={this.props.api} ></MyTable>
        }
        </Modal>
      </span>
    )
  }
}