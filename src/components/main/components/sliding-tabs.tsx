import * as React from "react";
import { Icon, Tabs } from "antd";
import {
    connect,
    MapStateToPropsParam,
    MapDispatchToPropsNonObject
} from "react-redux";
import { setTabActive, closeTab } from "../../../actions";
import { withRouter, RouteComponentProps } from "react-router-dom";
import routers from "../../../routes";
import { TabState } from "../../../reducers/tabs";

/** redux state */
const mapStateToProps = (state: any) => {
    return {
        tabs: state.tabs
    };
};

/** redux props */
const mapDispatchToProps = (dispatch: any) => {
    return {
        onTabClick: (tab: TabState) => {
            dispatch(setTabActive(tab));
        },
        onTabRemove: (tab: TabState) => {
            dispatch(closeTab(tab));
        }
    };
};
const TabPane = Tabs.TabPane;

/** props */
interface Props extends RouteComponentProps {
    /** 来自redux的state */
    tabs: TabState[];
    /** 点击事件 */
    onTabClick: (tab: TabState) => void;
    /** 关闭事件 */
    onTabRemove: (tab: TabState) => void;
}

class SlidingTabs extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    onTabsChange = (activeTab: string) => {
        this.props.history.push(activeTab);
    };

    onTabsEdit = (targetKey: string, action: string) => {
        if (action === "remove") {
            // 当前项的位置
            const index = this.props.tabs.findIndex(_ => _.path === targetKey);
            // 返回当前的上一页
            this.props.history.push(this.props.tabs[index - 1].path);
            this.props.onTabRemove({ path: targetKey });
        }
    };

    render() {
        const activeTab = this.props.tabs.find(
            _ => _.path === this.props.location.pathname
        );
        const tabs = this.props.tabs.map(i =>
            Object.assign({}, routers.find(_ => _.path === i.path), i)
        );
        return (
            <Tabs
                onChange={this.onTabsChange}
                onEdit={this.onTabsEdit}
                defaultActiveKey="/"
                type="editable-card"
                hideAdd
                activeKey={activeTab ? activeTab.path : "/"}
            >
                {tabs.map(_ => (
                    <TabPane
                        tab={_.name ? _.name : _.meta.name}
                        closable={_.path !== "/"}
                        key={_.path}
                    />
                ))}
            </Tabs>
        );
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(SlidingTabs)
);
