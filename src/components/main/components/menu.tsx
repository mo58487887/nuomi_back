import * as React from "react";
import {Menu, Icon } from 'antd';
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
import {routes} from '../../../routes/routes'
import { ClickParam } from "antd/lib/menu";

const SubMenu = Menu.SubMenu;

interface Props extends RouteComponentProps {
	/** 控制折叠 */
	collapsed: boolean
	/** 点击事件 */
	onClick:　(param: ClickParam) => void
}

const RoutesMenu: React.ComponentType<Props> = ({collapsed, onClick, location}: Props) => (
  <Menu mode="inline" selectedKeys={[location.pathname]} onClick={onClick} inlineCollapsed={collapsed}>
    {
      routes.filter(_ => !_.hideInMenu).map((_, i) => {
        if (_.children && _.children.length > 0 && _.children.filter(_ => !_.hideInMenu).length > 0) {
          return (
            <SubMenu key={`submenu_${i}`} title={<span><Icon type={_.meta.icon}></Icon><span>{_.meta.name}</span></span>}>
              {
                _.children.filter(_ => !_.hideInMenu).map((m, j) => <Menu.Item 
                  key={`${_.path}/${m.path}`}
                  title={m.meta && m.meta.name ? m.meta.name : '' }
                >
                  <Link to={`${_.path}/${m.path}`}>
                  <Icon type={m.meta.icon}></Icon>{m.meta.name}</Link>
                </Menu.Item>)
              }
            </SubMenu>
          )
        } else {
          return <Menu.Item key={`${_.path}`} title={_.meta.name}>
            <Link to={`${_.path}`}>
            <Icon type={_.meta.icon}></Icon><span>{!collapsed && _.meta.name}</span></Link>
          </Menu.Item>
        }
      })
    }
  </Menu>
)

export default withRouter(RoutesMenu)
