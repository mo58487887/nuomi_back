import * as React from "react";
import { Layout, Icon } from "antd";
const { Header, Sider, Content } = Layout;
import { withRouter, RouteComponentProps } from "react-router-dom";
import { connect } from "react-redux";
import { addTab } from "../../actions";
import Menu from "./components/menu";
import SlidingTabs from "./components/sliding-tabs";
import { TabState } from "../../reducers/tabs";
import "./index.less";
const mapDispatchToProps = (dispatch: any) => {
    return {
        addTab: (tab: TabState) => {
            dispatch(addTab(tab));
        }
    };
};

interface Props extends RouteComponentProps {
    addTab: (tab: TabState) => void;
}

interface State {
    collapsed: boolean;
}

class Main extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            collapsed: false
        };
    }
    /** 展开收缩 */
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    };
    /** 传递的菜单点击事件 */
    onMenuClick = ({ key }: { key: string }) => {
        this.props.addTab({
            path: key,
            name: ""
        });
    };
    componentDidMount() {
        const pathname = this.props.location.pathname;
        if (pathname !== "/login") {
            this.props.addTab({ path: pathname, name: "" });
        }
    }
    render() {
        const { collapsed } = this.state;
        const logoWidth = !collapsed ? 180 : 64,
            logoHeight = 64,
            logoStyle = !collapsed
                ? { width: "100%", height: "80px", padding: "8px 10px" }
                : { width: "100%", height: "80px", padding: "8px" };
        return (
            <Layout style={{ width: "100%", height: "100%" }}>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={collapsed}
                    style={{ background: "#ffffff" }}
                >
                    <div className="logo" style={logoStyle}>
                        <img
                            src={`http://tuimeizi.cn/random?w=${logoWidth}&h=${logoHeight}&s=0`}
                        />
                    </div>
                    <Menu collapsed={collapsed} onClick={this.onMenuClick} />
                </Sider>
                <Layout
                    style={{
                        width: collapsed
                            ? "calc(100% - 80px)"
                            : "calc(100% - 200px)"
                    }}
                >
                    <Header
                        style={{
                            background: "#fff",
                            padding: 0,
                            height: "40px"
                        }}
                    >
                        <Icon
                            className="trigger"
                            type={
                                this.state.collapsed
                                    ? "menu-unfold"
                                    : "menu-fold"
                            }
                            onClick={this.toggle}
                            style={{ fontSize: "30px" }}
                        />
                    </Header>
                    <Content>
                        <SlidingTabs />
                        <div
                            style={{
                                width: "100%",
                                // height: "100%",
                                padding: "25px"
                            }}
                        >
                            {this.props.children}
                        </div>
                    </Content>
                </Layout>
            </Layout>
        );
    }
}
export default withRouter(
    connect(
        null,
        mapDispatchToProps
    )(Main)
);
