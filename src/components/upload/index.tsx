import * as React from 'react'
import * as ReactDOM from "react-dom"
import "./index.less"
import { Upload, Icon } from 'antd';
import { RcFile } from 'antd/lib/upload';
import { randomString } from '../../libs/util';

interface Props {
	// onChange?: ({fileList, oldValue}: {fileList: RcFile[], oldValue: (string | string[])}) => void
	onChange?: (...param: any[]) => void
	/** 是否多文件 */
	multiple: boolean
	value?: string | string[]
	accept?: string
}

interface State {
	/** value值第一开始设计的时候本意是存放图片base64,antd自带这个功能,暂时没用 */
	value: string[]
	img: string[]
	list: RcFile[]
}

interface FileData {
	name: string
	file: MyTyping.ExParent<RcFile>
}

type fileOrImage = File | string | FileData

// value极为可能是接收string[]或者string, onChange提交两项,一项是string[],一项是fileList
export default class MyUpload extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props)
		const value: string[] = []
		if (typeof props.value === "string" && props.value) {
			value.push(...props.value.split(","))
		} else if (Array.isArray(props.value)) {
			value.push(...props.value)
		}
		this.state = {
			value,
			img: [],
			list: []
		}
	}

	onBeforeUpload = (file: RcFile, FileList: RcFile[]): boolean => {
		if (this.props.accept === "image/png, image/jpeg" || !this.props.accept ) {
			// const reader = new FileReader()
			// reader.readAsDataURL(file)
			// reader.onload = () => {
			// 	this.setState(pre => ({
			// 		value: pre.value.concat([reader.result as string])
			// 	}))
			// }
		}
		
		this.setState(pre => ({
			list: pre.list.concat(FileList)
		}), this.onChange)
		return false
	}

	onRemove = (f: RcFile) => {
		this.setState(pre => ({
			list: pre.list.filter(_ => _.name !== f.name)
		}), this.onChange)
	}

	onChange = () => {
		if (this.props.onChange) {
			this.props.onChange({
				fileList: this.state.list,
				oldValue: this.props.value
			})
		}
	}

	render() {
		const {multiple, accept} = this.props
		const {value, list} = this.state
		const hidden = {display: "none"}
		const uploadButton = (
			<div className="upload-button">
			  <Icon type={'plus'} />
			  <div>上传</div>
			</div>
		  );
		return (
			<span className="upload-wrap" style={((multiple && list.length < 10) || (!multiple && list.length === 0)) ? {} : {height: 0}}>
				<Upload fileList={list} listType={(!accept || /image/.test(accept)) ? "picture" : "text"} onRemove={this.onRemove} accept={accept ? accept : "image/png, image/jpeg"}  beforeUpload={this.onBeforeUpload}>
						{
							((multiple && list.length < 10) || (!multiple && list.length === 0)) &&  uploadButton
						}
				</Upload>
			</span>
		)
	}
}