import * as React from 'react'
import {message, Radio, Form, Button, Input, InputNumber, Checkbox} from 'antd'
import ajax from '../../libs/request'
import './index.less'
import { FormComponentProps } from 'antd/lib/form';
import { TableColumn } from '../table';
import { GetFieldDecoratorOptions } from 'antd/lib/form/Form';
import MyUpload from '../upload';
import {ChooseId} from '../choose-id';
const FormItem = Form.Item;
const { TextArea } = Input;
const CheckboxGroup  = Checkbox.Group

interface Props extends FormComponentProps {
	/**  */
	columns: Array<TableColumn<MyTyping.IAnyObject>>
	api: any
	data: any
	closeModal: any
	refresh: any
	handleCancel: any
}

interface GetFieldDecoratorInterface {
	id: string
	options: GetFieldDecoratorOptions
}

// interface GetFieldDecoratorOptions extends MyTyping.IAnyObject {

// }

class Edit extends React.Component<Props> {
  constructor(props: Props) {
    super(props)
    this.state = {
      data: {}
    }
  }

  componentDidMount() {
    if (this.props.data && this.props.data.topCategoryId) {
      this.setState({parentId: this.props.data.topCategoryId})
    }
  }
  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const _values = this.props.api.preHandle ? this.props.api.preHandle(values) : values
        if (!_values) return false
        ajax({
          url: this.props.api.url,
          data: this.props.data ? Object.assign({}, _values, {id: this.props.data.id}) : _values
        }).then(res => {
          if (res.code === 0) {
            message.success('保存成功')
            this.props.closeModal()
            this.props.refresh()
          } else {
			  message.warning(res.message)
		  }
        })
      }
    })
  }

  render() {
    const {data, columns, api} = this.props
    const preFomrItems = columns.filter(_ => _.input && (!_.input.renderType || (!data && _.input.renderType === 1) || (data && _.input.renderType === 2))).map((_: any) => ({
      key: _.dataIndex,
      label: _.title,
      style: _.style ? _.style : {},
      labelSpan: _.labelSpan ? _.labelSpan : 5,
      valueSpan: _.valueSpan ? _.valueSpan : 18,
      ..._.input
    })).map((_: any) => {
      if (!_.setting) _.setting = {}
      if (data && data.hasOwnProperty(_.key)) {
        _.setting.initialValue = data[_.key]
      } else {
        _.setting.initialValue = _.setting.initialValue ? _.setting.initialValue : ''
      }
      return _
    })
    const formItems = api.preRender ? api.preRender(preFomrItems, data) : preFomrItems 
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="edit-wrap" style={{width: 
      // formItems.length > 10 ? '1000px' : 
      '600px', marginTop: '10px'}}>
        <Form onSubmit={this.handleSubmit} className="login-form">
        {
          formItems.map((_: any) => (
            <div key={`${api.url}-${_.key}`} style={{minWidth: '450px'}}>
              {
                _.type === 'input' && 
                <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1 }}>
                  {
                    getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
                      <Input placeholder={_.placeholder ? _.placeholder : `请输入${_.label}`} />
                    )
                  }
                </FormItem>
              }
              {
                _.type === 'number' && 
                <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
                  {
                    getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
                      <InputNumber style={{width: '100%'}} placeholder={_.placeholder ? _.placeholder : `请输入${_.label}`} min={0}/>
                    )
                  }
                </FormItem>
              }
              {
                _.type === 'radio' && 
                <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
                  {
                    getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
                      <Radio.Group onChange={_.onSetFieldsValue ? v => _.onSetFieldsValue(v, this.props.form) : null} name={_.key} buttonStyle="outline" options={_.options}></Radio.Group>
                    )
                  }
                </FormItem>
              }
              {
                _.type === 'checkgroup' && 
                <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
                  {
                    getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
                      <CheckboxGroup onChange={_.onSetFieldsValue ? v => _.onSetFieldsValue(v, this.props.form) : null} options={_.options}></CheckboxGroup>
                    )
                  }
                </FormItem>
              }
              {
                _.type === 'textarea' && 
                <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
                  {
                    getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
                      <TextArea rows={2}/>
                    )
                  }
                </FormItem>
			  }
			  {
				  _.type === "upload" && 
				  <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
					{
					  getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
						<MyUpload multiple={_.uploadMultiple ? true : false} />
					  )
					}
				  </FormItem>
			  }
              {
                _.type === 'render' && 
                <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
                  {
                    getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
                      _.editRender
                    )
                  }
                </FormItem>
			  }
			  {
				  _.type === "chooseId" && 
				  <FormItem key={_.key} label={_.label} labelCol={{ span: _.labelSpan }}  wrapperCol={{ span: _.valueSpan,  offset: 1  }}>
					{
					  getFieldDecorator<GetFieldDecoratorInterface>(_.key, _.setting ? _.setting : {})(
						<ChooseId {..._.chooseOptions} />
					  )
					}
				  </FormItem>
			  }
            </div>
          ))
        }
        <FormItem wrapperCol={{ span: 19}} style={{width: 'calc(100% - 48px)', textAlign: 'right'}}>
          <Button style={{width: '100px', marginRight: '10px'}} onClick={this.props.handleCancel} className="login-form-button">
            取消
          </Button>
          <Button style={{width: '100px'}} type="primary" htmlType="submit" className="login-form-button">
            保存
          </Button>
        </FormItem>
        </Form>
      </div>
    )
  }
}
export default Form.create<Props>()(Edit);