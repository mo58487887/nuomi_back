import * as React from "react";
import { Table, Button, Switch, message, Modal } from "antd";
import ajax from "../../libs/request";
import "./index.less";
import Edit from "../edit";
import Info from "../info";
import { ColumnProps, TableProps } from "antd/lib/table";
import { GetFieldDecoratorOptions } from "antd/lib/form/Form";
import { CheckboxOptionType } from "antd/lib/checkbox/Group";
import { ChooseIdProps } from "../choose-id";

export interface MyTableProps<T> extends TableProps<T> {
    /** 列数据 */
    columns: Array<TableColumn<T>>;
    /** api信息 */
    api: TableApi;
    /** 页面缓存传进来的数据 */
    state?: any;
    /** 卸载函数 */
    preUnmount?: (state: any) => void;
    /** 预设的pageSize */
	pageSize?: number;
	/** 列表唯一key值 */
	rowKey?: string
	/** 自定义header */
	CustomHeaderButton?: typeof React.Component
}

/** 扩展列数据 */
export interface TableColumn<T> extends ColumnProps<T> {
    /** 是否渲染进表格 */
    renderInTable?: boolean;
    /** 是否渲染进详情 */
    renderInInfo?: boolean;
    /** 带有组件实例this的扩展render */
    bindRender?: (t: String, r: T, i: number, self: any) => React.ReactNode;
	/** 编辑格式 */
	input?: {
		/** 输入类型 */
		type: "input" | "number" | "radio" | "render" | "upload" | "chooseId",
		/** 在radio等类型下的options */
		options?: CheckboxOptionType[],
		/** 提示 */
		placeholder?: string,
		/** 校验规则 */
		setting? : GetFieldDecoratorOptions
		/** 自定义表单组件 */
		editRender?: React.ReactNode,
		/** 当type为upload时有效,是否开启多图上传 */
		uploadMultiple?: boolean,
		/** 当type为chooseId时有效 */
		chooseOptions?: ChooseIdProps
		/** 渲染编辑的类型,不填为既修改又创建的项,1为只修改,2为只创建 */
		renderType?: 1 | 2
	}
}

/** 表单数据 */
export interface TableApi {
    list: ApiItem;
    enable?: ApiItem;
    disable?: ApiItem;
    edit?: ApiItem;
    create?: ApiItem;
    remove?: ApiItem;
    info?: ApiItem;
}

export interface ApiItem {
    /** 接口地址 */
    url: string;
    /** 渲染前数据处理,只在edit/create中有效 */
    preRender?: (columns: Array<TableColumn<any>>, data: any) => Array<any>;
    /**  */
    preHandle?: (data: any) => any;
    /** 覆盖点击事件 */
    onClick?: (data?: any, type?: string, self?: any) => void;
    /** 请求携带的额外参数, */
    extraParm?: MyTyping.IAnyObject;
}

interface State {
    /** 是否加载中 */
    loading: boolean;
    /** ??? */
    stateColumns: any[];
    /** 当亲页数 */
    currentPage: number;
    /** 当前页数据 */
    pageData: any[];
    /** 列表总数 */
    total: number;
    /** 用于修改/详情数据的预先展示 */
    itemData: any;
    /** 弹窗控制 */
    modalvisible: boolean;
    /** 创建或者编辑的请求地址 */
    editApi: ApiItem | string | null;
}

export class MyTable extends React.Component<
    MyTableProps<MyTyping.IAnyObject>,
    State
> {
    constructor(props: MyTableProps<MyTyping.IAnyObject>) {
        super(props);
        this.state = {
            loading: false,
            stateColumns: [],
            currentPage: 1,
            pageData: [],
            total: 0,
            itemData: null,
            modalvisible: false,
            editApi: null
        };
    }
    componentDidMount() {
        // 处理传进来的columns
        const _columns: Array<
            ColumnProps<MyTyping.IAnyObject>
        > = this.props.columns
            .filter(_ => _.renderInTable === true)
            .map(_ => {
                const _c: MyTyping.ExParent<
                    TableColumn<MyTyping.IAnyObject>
                > = {};
                if (_.render) _c.render = _.render;
                if (_.bindRender)
                    _c.render = (t, r, i) => _.bindRender(t, r, i, this);
                if (_.className) _c.className = _.className;
                if (_.fixed) _c.fixed = _.fixed;
                _c.align = _.align ? _.align : "center";
                return Object.assign({}, _, _c);
            });
        const api = this.props.api;
        // 按钮样式
        const style = {
            height: "24px",
            verticalAlign: "middle",
            marginRight: "3px"
        };
        // 按钮生成
        const handleColumn: MyTyping.ExParent<
            TableColumn<MyTyping.IAnyObject>
        > = {
            title: "操作",
            dataIndex: "handle",
            align: "center",
            width: 165,
            render: (text, record, index) => (
                <span>
                    {api.enable && (
                        <Switch
                            style={style}
                            checkedChildren="启用"
                            unCheckedChildren="禁用"
                            checked={record.status === 1}
                            data-index={index}
                            onChange={boo => this.onSwichChane(boo, record.id)}
                        />
                    )}
                    {api.info && (
                        <Button
                            style={style}
                            size="small"
                            type="primary"
                            data-index={index}
                            onClick={() => this.onInfoClick(record)}
                        >
                            详情
                        </Button>
                    )}
                    {api.edit && (
                        <Button
                            style={style}
                            size="small"
                            type="primary"
                            data-index={index}
                            onClick={() => this.onEditClick(record)}
                        >
                            编辑
                        </Button>
                    )}
                    {api.remove && (
                        <Button
                            style={style}
                            size="small"
                            type="danger"
                            data-index={index}
                            onClick={() =>
                                this.onRemoveClick(record.id, record)
                            }
                        >
                            删除
                        </Button>
                    )}
                </span>
            )
        };
        if (api.enable || api.info || api.edit || api.remove)
            _columns.push(handleColumn);
        this.setState({
            stateColumns: _columns
        });
        // 加载缓存数据
        if (this.props.state && JSON.stringify(this.props.state) !== "{}") {
            this.setState(
                pre => Object.assign({}, pre, this.props.state),
                this.getList
            );
            return;
        }
        this.getList();
    }
    componentWillUnmount() {
        const { currentPage } = this.state;
        if (this.props.preUnmount) {
            this.props.preUnmount({
                currentPage
            });
        }
    }
    // 切换启用或者禁用
    onSwichChane = (boo: boolean, id: string | number) => {
        ajax({
            url: boo ? this.props.api.enable.url : this.props.api.disable.url,
            data: { id }
        }).then(res => {
            if (res.code === 200) {
                message.success(boo ? "启用成功" : "禁用成功");
                this.setState(pre => ({
                    pageData: pre.pageData.map(_ =>
                        _.id === id ? { ..._, status: boo ? 1 : 0 } : _
                    )
                }));
            }
        });
    };

    /** 新建条目 */
    onCreateClick = () => {
        this.onEditClick(null, "create");
    };
    onEditClick = (data: any, type = "edit") => {
        // if (this.props.api.edit.click) {
        //     this.props.api.edit.click(data, type, this);
        //     return;
        // }
        const editApi =
            type === "edit" ? this.props.api.edit : this.props.api.create;
        this.setState({
            editApi
        });
        if (type === "edit") {
            ajax({
                url: this.props.api.info.url,
                data: { id: data.id }
            }).then(res => {
                if (res.code === 0) {
                    this.setState({
                        modalvisible: true,
                        itemData: res.body
                    });
                } else {
                    this.setState({
                        modalvisible: true,
                        itemData: data
                    });
                }
            });
        } else {
            this.setState({
                modalvisible: true,
                itemData: data
            });
        }
    };

    onInfoClick = (data: MyTyping.IAnyObject) => {
        ajax({
            url: this.props.api.info.url,
            data: { id: data.id }
        }).then(res => {
            if (res.code === 0) {
                this.setState({
                    modalvisible: true,
                    itemData: res.body
                });
            } else {
                this.setState({
                    modalvisible: true,
                    itemData: data
                });
            }
        });
    };

    /** 单行删除事件 */
    onRemoveClick = (id: string | number, r: MyTyping.IAnyObject) => {
        if (this.props.api.remove.onClick) {
            this.props.api.remove.onClick(r, undefined, this);
            return;
        }
        Modal.confirm({
            content: "是否删除该项",
            okText: "确认",
            cancelText: "取消",
            onOk: () => {
                ajax({
                    url: this.props.api.remove.url,
                    data: { id }
                }).then(res => {
                    if (res.code === 200) {
                        message.success("已删除");
                        this.getList();
                    }
                });
            }
        });
    };

    /** 关闭弹窗 */
    setModalvisible = () => {
        this.setState({
            modalvisible: false,
            itemData: null,
            editApi: null
        });
    };

    /** 列表刷新 */
    getList = (index = this.state.currentPage) => {
        this.setState(
            {
                loading: true
            },
            () => {
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 500);
            }
        );
        const api = this.props.api.list;
        const { currentPage } = this.state;
        if (!api) return;
        /** 筛选数据,项目预留 */
        const sendFilterData = {};
        // for (let _k in filterData) {
        //     if (filterData[_k] !== "") {
        //         sendFilterData[_k] = filterData[_k];
        //     }
        // }
        const sendData = api.extraParm
            ? Object.assign({}, api.extraParm, sendFilterData)
            : sendFilterData;
        if (this.props.api.list) {
			sendData.page = index;
			sendData.row = 10
			sendData.rows = 10
		}
        ajax({
            url: api.url,
            data: sendData
        }).then(res => {
            const { code, data } = res;
            if (code === 0) {
                // if (Array.isArray(data)) {
                //     if (data.length >= 0) {
                //         // const afterHandleData = api.filter
                //         //     ? api.filter(data)
                //         //     : data;
                //         this.setState({
                //             pageData: data,
                //             total: data.length
                //         });
                //     }
                //     return;
                // }
                // 在比如删除操作后列表刷新,但当前只有一条数据时,自动加载并返回上一页
                if (data.list.length === 0) {
                    // 判断是否是第一页,是的话表示列表就是空
                    if (currentPage === 1) {
                        this.setState({
                            pageData: []
                        });
                    } else {
                        this.setState(
                            pre => ({
                                currentPage: pre.currentPage - 1
                            }),
                            this.getList
                        );
                    }
                    return;
                } else {
                    const afterHandleData = data.list;
                    this.setState({
                        pageData: afterHandleData,
                        currentPage: index,
                        total: data.total
                    });
                }
            }
        });
    };
    // onFilterChange = data => {
    //     this.setState(
    //         {
    //             filterData: data
    //         },
    //         () => this.getList(1)
    //     );
    // };
    onPageChange = (pageNum: number) => {
        this.getList(pageNum);
    };
    render() {
        const {
            loading,
            stateColumns,
            currentPage,
            total,
            pageData,
            itemData,
            modalvisible,
            editApi
            // filterData
        } = this.state;
        const paginationConfig = {
            current: currentPage,
            total: total,
            onChange: this.onPageChange,
            pageSize: this.props.pageSize ? this.props.pageSize : 10
        };
        // const rowSelection = this.props.rowSelection
        //     ? this.props.rowSelection
        //     : null;
        const {api, rowKey, CustomHeaderButton} = this.props;
        // const dataSource = api.all
        //     ? pageData.filter(
        //           (_, i) => i >= (currentPage - 1) * 10 && i < currentPage * 10
        //       )
        //     : pageData;
        const dataSource = pageData;
        const propsColumns = this.props.columns;
        // const listFilter =
        //     api.list && api.list.filters
        //         ? api.list.filters
        //         : api.all && api.all.filters
        //         ? api.all.filters
        //         : null;
        return (
            <div className="my-table-wrap">
                <div className="table-header clearfix">
                    {/* {
			listFilter && 
			<HeaderFilters filterData={filterData} onChange={this.onFilterChange} options={listFilter} />
			} */}
                    <div className="common-handle">
                        {api.create && (
                            <Button
                                type="primary"
                                style={{ marginRight: "10px" }}
                                onClick={this.onCreateClick}
                            >
                                创建
                            </Button>
                        )}
                        {api.list && (
                            <Button onClick={() => this.getList(1)}>
                                刷新
                            </Button>
						)}
						{
							CustomHeaderButton && <CustomHeaderButton current={this} />
						}
                    </div>
                </div>
                <Table
                    columns={stateColumns}
                    pagination={paginationConfig}
                    loading={loading}
                    // rowSelection={rowSelection}
                    dataSource={dataSource}
                    rowKey={r => `${r[rowKey]}`}
                    scroll={{ x: true }}
                    indentSize={25}
                    defaultExpandAllRows
                />
                <Modal
                    destroyOnClose
                    footer={null}
                    width={
                        // editApi ? propsColumns.filter(_ => _.input).length > 10 ? 1100 : 500 : propsColumns.filter(_ => _.renderInInfo).length > 10 ? 1100 :
                        500
                    }
                    style={{ top: "10px" }}
                    visible={modalvisible}
                    onCancel={this.setModalvisible}
                    maskClosable={editApi ? false : true}
                >
                    {editApi && modalvisible && (
                        <Edit
                            columns={propsColumns}
                            api={editApi}
                            data={itemData}
                            closeModal={this.setModalvisible}
                            refresh={this.getList}
                            handleCancel={this.setModalvisible}
                        />
                    )}
                    {!editApi && modalvisible && (
                        <Info columns={propsColumns} data={itemData} />
                    )}
                </Modal>
            </div>
        );
    }
}
