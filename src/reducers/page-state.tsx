export interface PageState {
    /** 路径 */
    path: string;
    /** 状态 */
    state: any;
}
interface Action extends PageState {
    type: string;
}
type pageStateReducer = (
    state: Array<PageState>,
    action: Action
) => Array<PageState>;

export const pageState: pageStateReducer = (state = [], action) => {
    switch (action.type) {
        case "SET_PAGE_STATE":
            if (state.find(_ => _.path === action.path)) {
                return state.map(_ =>
                    _.path === action.path
                        ? { path: _.path, state: action.state }
                        : _
                );
            } else {
                return state.concat([
                    { path: action.path, state: action.state }
                ]);
            }
        case "REMOVE_PAGE_STATE":
            return state.filter(_ => _.path !== action.path);
        default:
            return state;
    }
};
