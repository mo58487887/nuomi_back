interface Action {
    type: string;
    tab: TabState;
}
export interface TabState {
    name?: string;
    path?: string;
    isActive?: boolean;
}
export const tabs = (
    state = [{ name: "首页", path: "/", isActive: true }],
    action: Action
) => {
    switch (action.type) {
        case "ADD_TAB":
            const oldItem = state.find(_ => _.path === action.tab.path);
            if (oldItem) {
                return state;
            }
            return [...state, action.tab];
        case "CLOSE_TAB":
            const item = state.find(tab => tab.path === action.tab.path);
            const index = state.findIndex(tab => tab.path === action.tab.path);
            if (item.isActive) {
                return state
                    .map((_, i) =>
                        i === index - 1
                            ? Object.assign({}, _, { isActive: true })
                            : _
                    )
                    .filter(tab => tab.path !== action.tab.path);
            }
            return state.filter(tab => tab.path !== action.tab.path);
        case "SET_ACTIVE_TAB":
            return state.map(_ =>
                _.path === action.tab.path
                    ? Object.assign({}, _, { isActive: true })
                    : Object.assign({}, _, { isActive: false })
            );
        case "SET_TAB":
            return state.map(_ =>
                _.path === action.tab.path
                    ? Object.assign({}, _, action.tab)
                    : _
            );
        default:
            return state;
    }
};
