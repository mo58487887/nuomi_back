interface Action {
    type: string;
    token: string;
}

const users = (state = { token: "" }, action: Action) => {
    switch (action.type) {
        case "SET_TOKEN":
            return Object.assign({}, state, { token: action.token });
        default:
            return state;
    }
};

export default users;
