import { combineReducers } from "redux";
import { tabs } from "./tabs";
import user from "./user";
import { pageState } from "./page-state";

export default combineReducers({
    tabs,
    user,
    pageState
});
