import { message } from "antd";

interface Config {
    method?: string | "GET" | "POST";
    headers?: any;
    data?: any;
    url: string;
    dataType?: "json" | "text";
}

function ajax(config: Config): Promise<any> {
    config.method = config.method ? config.method : "POST";
    config.headers = config.headers ? config.headers : {};
    //   if (config.method === 'POST' && !config.headers.hasOwnProperty('Content-Type')) config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    const formData = new FormData();
    if (config.data && typeof config.data !== "string") {
        for (const key in config.data) {
            if (config.data.hasOwnProperty(key)) {
                const element = config.data[key];
                if (Array.isArray(element)) {
                    element.forEach(_ => {
                        if (_ instanceof File) {
                            formData.append(key, _);
                        } else if (_.name) {
                            formData.append(key, _.file, _.name);
                        }
                    });
                } else {
                    formData.append(key, element);
                }
            }
        }
    }
    if (!config.hasOwnProperty("data")) {
        config.data = {};
	}
	// if (formData.getAll("goodsDetail").length > 0) {
	// 	console.log(formData.getAll("goodsDetail"))
	// 	return
	// }
    const _url = config.url;
    return fetch(_url, {
        body: formData, // must match 'Content-Type' header
        headers: config.headers,
        method: config.method, // *GET, POST, PUT, DELETE, etc.
        mode: "no-cors"
    })
        .then(response => {
            if (!config.dataType || config.dataType === "json") {
                return response.json();
            } else if (config.dataType === "text") {
                return response.text();
            }
        })
        .then(res => {
            if (res.code === 2) {
                // 超时状态码
                window.location.hash = "/login";
                return Promise.reject();
            } else {
                return res;
            }
        })
        .catch(err => {
            console.log(err);
            message.warning("请求异常,请重试,或者联系维护人员");
        }); // parses response to JSON
}
export default ajax;
