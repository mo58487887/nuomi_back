/**
 * @file 常用工具集合
 * @author Monet
 */
/**
 * @description 手机号验证
 * @param {string} str 需要验证的字符串
 * @returns {boolean}
 */
export function isPhone(str: string): boolean {
    const myreg = /^[1][3,4,5,6,7,8][0-9]{9}$/;
    return myreg.test(str);
}

/**
 * @description 生成占位图，前期开发用
 * @param {(number | string)} w 占位宽度
 * @param {(number | string)=} h 占位高度
 * @returns {string} 返回占位图地址
 */
export const placeImage = (w: number | string, h = w, type = "random"): string =>
    `https://tuimeizi.cn/${type}?w=${w}&h=${h}&s=0`;
/**
 *  @description 密码验证
 *  @param {string} str 密码
 */
export  function isPassword (str: string): boolean {
  const myReg = /^[a-zA-Z0-9]{8,16}$/
  return myReg.test(str)
}


/**
 *  产生一定长度的随机字符串
 */
export function randomString (len: number = 32): string {
  var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  var maxPos = chars.length;
  var pwd = '';
  for (let i = 0; i < len; i++) {
    pwd += chars.charAt(Math.floor(Math.random() * maxPos));
  }
  return pwd;
}

const tomorrow = () => {
  const date = new Date()
  const day = date.getDate()
  date.setDate(day + 1)
  return date
}

/**
 *  格式化时间，2018-12-24 12：32：26
 */
export const formatTime = (time: number): string => {
  if (!time) {
    return ''
  }
  const date = new Date(time)
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

/**
 *  格式化时间，2018-12-24 12：32：26
 */
export const formatMinutes = (time: number): string => {
  if (!time) {
    return ''
  }
  const date = new Date(time)
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute].map(formatNumber).join(':')
}

/**
 *  格式化时间 2018-12-24
 */
export const formatDate = (time: number): string => {
  if (!time) {
    return ''
  }
  const date = time ? new Date(time) : tomorrow()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  return [year, month, day].map(formatNumber).join('-')
}

/**
 *  格式化时间 2018/12/24
 */
export const formatDateSlash = (time: number): string => {
  if (!time) {
    return ''
  }
  const date = time ? new Date(time) : tomorrow()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return [year, month, day].map(formatNumber).join('/')
}

export const formatNumber = (n: number): string => {
  const _n = n.toString()
  return _n[1] ? _n : '0' + _n
}

// export const sign = (url,signData) => {
//   const _name = randomString();
//   const _ext = url.slice(((url.lastIndexOf(".") - 1) >>> 0) + 2);
//   const _filename = _name + "." + _ext;
//   const _sign = {
//     headers:{'Content-Type':'multipart/form-data'},
//     Filename: signData.dir + "/" + _filename,
//     key: signData.dir + "/" + _filename,
//     policy: signData.policy,
//     OSSAccessKeyId: signData.accessId,
//     success_action_status: "200", //让服务端返回200，不然，默认会返回204
//     signature: signData.signature
//   }
//   return {
//     signData: _sign,
//     url: `${signData.host}/${signData.dir}/${_filename}`
//   }
// }

export function parseURL(url: string) {
  var parser = document.createElement("a"),
      search: MyTyping.IAnyObject = {},
      queries,key, value
      
  parser.href = url
  queries = parser.search.replace(/^\?/, "").split("&")
  queries.forEach((query) => {
    key = query.split("=")[0]
    value = query.split("=")[1]
    search[key] = value
  })
  
  return {
    protocol: parser.protocol,
    host: parser.host,
    hostname: parser.hostname,
    port: parser.port,
    pathname: parser.pathname,
    search: search,
    hash: parser.hash,
    origin: parser.origin
  }
}

export function download(url: string) {
  const downForm = document.createElement('form')
  const {search, pathname, origin} = parseURL(url)
  if (JSON.stringify(search) !== '{}') {
    for (const key in search) {
      const hiInput = document.createElement('input')
      hiInput.type = 'hidden'
      hiInput.name = key
      hiInput.value = decodeURIComponent(search[key])
      downForm.appendChild(hiInput)
    }
  }
  downForm.method = 'GET'
  downForm.style.display = "none"
  downForm.target = "_blank"
  downForm.action = origin + pathname
  console.log(downForm)
  document.body.appendChild(downForm)
  downForm.submit()
  document.body.removeChild(downForm)
}

/**
 * 函数防抖
 * @param fn 
 * @param interval 
 */
export function debounce<F extends (...params: any[]) => void>(fn: F, delay: number) {
	let timeoutID: number = null;
	return function(this: any, ...args: any[]) {
	  clearTimeout(timeoutID);
	  timeoutID = window.setTimeout(() => fn.apply(this, args), delay);
	} as F;
  }