import { TabState } from "../reducers/tabs";

/*
 *  tab控制
 */
export const addTab = (tab: TabState) => ({
    type: "ADD_TAB",
    tab
});
export const closeTab = (tab: TabState) => ({
    type: "CLOSE_TAB",
    tab
});
export const setTabActive = (tab: TabState) => ({
    type: "SET_ACTIVE_TAB",
    tab
});
export const setTab = (tab: TabState) => ({
    type: "SET_TAB",
    tab
});

/*
 *  用户权限
 */
export const setToken = (token: string) => ({
    type: "SET_TOKEN",
    token
});
/**
 * @description keep-alive
 */
/** 新增页面缓存 */
export const setPageState = ({
    path,
    state
}: {
    path: string;
    state: any;
}) => ({
    type: "SET_PAGE_STATE",
    path,
    state
});
/** 清除页面缓存 */
export const removePageState = ({ path }: { path: string }) => ({
    type: "REMOVE_PAGE_STATE",
    path
});
