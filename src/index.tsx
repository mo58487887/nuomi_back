import * as React from "react";
import { render } from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { Route, Switch, HashRouter as Router } from "react-router-dom";
import App from "./app";
import rootReducer from "./reducers";
import routes from "./routes";
import Login from "./pages/Login";

const store = createStore(
    rootReducer,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

render(
    <Provider store={store}>
        <Router>
            <div style={{ height: "100%" }}>
                <App>
                    <Switch>
                        {routes.map((r, i) => (
                            <Route
                                key={r.path}
                                path={r.path}
                                exact={r.path === "/"}
                                component={r.component}
                            />
                        ))}
                    </Switch>
                </App>
                <Route path="/login" key="login" component={Login} />
            </div>
        </Router>
    </Provider>,
    document.getElementById("root")
);
