import * as React from 'react'
import { connect } from "react-redux";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { removePageState, setPageState } from '../actions';
import { PageState } from '../reducers/page-state';
const mapStateProps = (state: any) => {
  return {
    propsState: state.pageState
  }
}
const mapDispatchProps = (dispatch: any) => {
  return {

    onRemovePageState: (path: string) => {
      dispatch(removePageState({path}))
	},
	
    onSetPageState: ({path, state}: PageState) => {
      dispatch(setPageState({path, state}))
    }
  }
}
interface Props extends RouteComponentProps {
	onSetPageState: (pageState: PageState) => void
	propsState: Array<PageState>
}
export default function (WrapComponent: typeof React.Component, cache = true) {
  class WithCache extends React.Component<Props> {
    preUnmount = (state: any) => {
      if (cache) this.props.onSetPageState({path: this.props.location.pathname, state})
    }
    render() {
      const data = this.props.propsState.find(_ => _.path === this.props.location.pathname)
	  const propsStateItem = data ? data.state : {}
      return <WrapComponent  state={propsStateItem} preUnmount={this.preUnmount} {...this.props}  />
    }
  }
  return withRouter(connect(mapStateProps, mapDispatchProps)(WithCache))
}