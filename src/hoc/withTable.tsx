import * as React from 'react'
import withCache from './withCache'
import { MyTableProps, MyTable } from '../components/table';


export default function (props: MyTableProps<MyTyping.IAnyObject>, cache = true) {
  class WithTable extends React.Component {
    render() {
      return <MyTable {...props} {...this.props}  />
    }
  }
  return withCache(WithTable, cache)
}
