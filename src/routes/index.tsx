import { routes, RoutePage } from "./routes";

const router: RoutePage[] = [];

function loop(routeObj: RoutePage, rootPath?: string) {
    const path = rootPath ? rootPath + "/" + routeObj.path : routeObj.path;
    router.push({ path, component: routeObj.component, meta: routeObj.meta });
    if (!routeObj.children || !routeObj.children.length) {
        return;
    } else {
        routeObj.children.forEach(_ => {
            loop(_, path);
        });
    }
}
routes.forEach(i => {
    loop(i);
});

export default router.filter(_ => _.component);
