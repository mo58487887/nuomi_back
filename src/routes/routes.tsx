import IndexPage from "../pages/Index";
import GoodsPage from "../pages/Goods";
import { FunctionComponent, ComponentClass } from "react";
import { RouteComponentProps, StaticContext } from "react-router";
import Supplier from "../pages/Supplier";
import Order from "../pages/Order";

export interface RoutePage {
    /** 路径 */
    path: string;
    component:
        | FunctionComponent<any>
        | ComponentClass<any, any>
        | ComponentClass<RouteComponentProps<any, StaticContext, any>, any>
        | FunctionComponent<RouteComponentProps<any, StaticContext, any>>;
    meta?: RouteMeta;
    children?: Array<RoutePage>;
    hideInMenu?: boolean;
    access?: Array<AccessType>;
}

type AccessType = "admin" | "normal";

interface RouteMeta {
    name: string;
    icon: string;
}

export const routes: Array<RoutePage> = [
    {
        path: "/",
        component: IndexPage,
        meta: {
            name: "首页",
            icon: "meh"
        }
	},
	{
		path: "/supplier",
		component: Supplier,
		meta: {
            name: "供货商信息",
            icon: "meh"
        }
	},
    {
        path: "/goods",
        component: GoodsPage,
        meta: {
            name: "商品管理",
            icon: "meh"
        }
    },
    {
        path: "/order",
        component: Order,
        meta: {
            name: "订单管理",
            icon: "meh"
        }
    },
];
